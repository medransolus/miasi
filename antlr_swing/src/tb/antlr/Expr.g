grammar Expr;

options
{
  output=AST;
  ASTLabelType=CommonTree;
}

@header
{
  package tb.antlr;
}

@lexer::header
{
  package tb.antlr;
}

prog
  : (stat | codeBlock)+ EOF!;

// Declaration of inner code block with {...}
codeBlock
  : LB (stat | codeBlock)* RB;

stat
  : expr NL -> expr
  | VAR ID (ASSIGN expr)? NL -> ^(VAR ID) ^(ASSIGN ID expr)?
  | ID ASSIGN expr NL -> ^(ASSIGN ID expr)
  | PRINT expr NL -> ^(PRINT expr)
  | NL ->
  ;

// AND and OR separated 'cause of it's precedense
expr
  : andExpr
  ( OR^ andExpr)*
  ;
  
andExpr
  : shiftExpr
  ( AND^ shiftExpr)*
  ;
  
shiftExpr
  : addExpr
  ( SHL^ addExpr
  | SHR^ addExpr
  )*
  ;

addExpr
  : mulExpr
  ( PLUS^ mulExpr
  | MINUS^ mulExpr
  )*
  ;

mulExpr
  : powExpr
  ( MUL^ powExpr
  | DIV^ powExpr
  | MOD^ powExpr
  )*
  ;

powExpr
  : atom
  ( POW^ atom
  )*
  ;

atom
  : INT
  | ID
  | LP! expr RP!
  ;

VAR   :'var';
PRINT : 'print';

ID  : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;
INT : '0'..'9'+;
NL  : '\r'? '\n' ;
WS  : (' ' | '\t')+ {$channel = HIDDEN;};

LP     : '(';
RP     : ')';
LB     : '{';
RB     : '}';
ASSIGN : '=';
PLUS   : '+';
MINUS  : '-';
MUL    : '*';
DIV    : '/';
POW    : '^';
MOD    : '%';
OR     : '|';
AND    : '&';
SHL    : '<<';
SHR    : '>>';