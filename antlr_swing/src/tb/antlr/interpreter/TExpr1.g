tree grammar TExpr1;

options
{
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header
{
  package tb.antlr.interpreter;
}

prog
  : (^(PRINT e=expr) {print($e.text + " = " + $e.out.toString());}
  | declare | codeBlock | expr)*;

codeBlock
  : LB { enterScope();}
  | RB { leaveScope();}
  ;

declare
  : ^(VAR i1=ID) {declareInt($i1.text);};
  
expr returns [Integer out]
	      : ^(PLUS   e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS  e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL    e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV    e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(POW    e1=expr e2=expr) {$out = pow($e1.out, $e2.out);}
        | ^(MOD    e1=expr e2=expr) {$out = mod($e1.out, $e2.out);}
        | ^(AND    e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(OR     e1=expr e2=expr) {$out = $e1.out | $e2.out;}
        | ^(SHL    e1=expr e2=expr) {$out = $e1.out << $e2.out;}
        | ^(SHR    e1=expr e2=expr) {$out = $e1.out >> $e2.out;}
        | ^(ASSIGN i1=ID   e2=expr) {$out = setInt($i1.text, $e2.out);}
        | ID                        {$out = getInt($ID.text);}
        | INT                       {$out = parseInt($INT.text);}
        ;