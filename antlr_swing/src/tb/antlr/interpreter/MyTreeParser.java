package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;;

public class MyTreeParser extends TreeParser
{
	LocalSymbols symbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input)
    {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state)
    {
        super(input, state);
    }

    protected void print(String text)
    {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected void enterScope()
    {
    	symbols.enterScope();
    }
    
    protected void leaveScope()
    {
    	symbols.leaveScope();
    }
        
    protected void declareInt(String id)
    {
    	symbols.newSymbol(id);
    }
    
    protected Integer div(Integer val1, Integer val2)
    {
    	if (val2 == 0)
    		throw new RuntimeException("Cannot divide by zero!");
    	return val1 / val2;
    }
    
    protected Integer pow(Integer val1, Integer val2)
    {
    	return (int)Math.pow(val1, val2);
    }
    
    protected Integer mod(Integer val1, Integer val2)
    {
    	return val1 % val2;
    }

	protected Integer parseInt(String text)
	{
		return Integer.parseInt(text);
	}

	protected Integer setInt(String id, Integer value)
	{
		symbols.setSymbol(id, value);
		return value;
	}

	protected Integer getInt(String id)
	{
		return symbols.getSymbol(id);
	}	
}
