tree grammar TExpr3;

options
{
  tokenVocab=Expr;
  ASTLabelType=CommonTree;
  output=template;
  superClass = TreeParserTmpl;
}

@header
{
  package tb.antlr.kompilator;
}

@members
{
  Integer localTag = 0;
  Integer currentTag;
}

prog
  : (e+=expr | d+=decl)* -> start(name={$e}, declare={$d});

decl
  : ^(VAR id=ID) {addVariable($ID.text, localTag);} -> declare(id={$ID.text}, t={localTag.toString()});
    catch[RuntimeException e] {}

expr
  : ^(PLUS   e1=expr e2=expr) -> add(p1={$e1.st}, p2={$e2.st})
  | ^(MINUS  e1=expr e2=expr) -> sub(p1={$e1.st}, p2={$e2.st})
  | ^(MUL    e1=expr e2=expr) -> mul(p1={$e1.st}, p2={$e2.st})
  | ^(DIV    e1=expr e2=expr) -> div(p1={$e1.st}, p2={$e2.st})
  | ^(MOD    e1=expr e2=expr) -> mod(p1={$e1.st}, p2={$e2.st})
  | ^(OR     e1=expr e2=expr) ->  or(p1={$e1.st}, p2={$e2.st})
  | ^(AND    e1=expr e2=expr) -> and(p1={$e1.st}, p2={$e2.st})
  | ^(ASSIGN id=ID   e2=expr) {currentTag=getVariableTag($ID.text, localTag);} -> set(id={$ID.text}, t={currentTag.toString()}, p={$e2.st})
  | ID                        {currentTag=getVariableTag($ID.text, localTag);} -> var(id={$ID.text}, t={currentTag.toString()})
  | INT -> int(i={$INT.text})
  | LB {++localTag;}
  | RB {--localTag;}
  ;