package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;
import tb.antlr.symbolTable.*;

public class TreeParserTmpl extends TreeParser
{
	GlobalSymbols globals = new GlobalSymbols();
	
	String makeName(String id, Integer tag) { return id + "#" + tag.toString(); }
	void addVariable(String name, Integer tag) { globals.newSymbol(makeName(name, tag)); }

	Integer getVariableTag(String name, Integer localTag)
	{
		for (int i = localTag; i >= 0; --i)
			if (globals.hasSymbol(makeName(name, i)))
				return i;
		throw new RuntimeException("Cannot find variable named <"
				+ name + "> from " + localTag.toString() + " tag level.");
	}
	
	public TreeParserTmpl(TreeNodeStream input) { super(input); }
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) { super(input, state); }

}