grammar lab1;

plik
     : expr* EOF
     ;

expr
    : term (PLUS term
    | MINUS term)* NL
    ;

term
    : atom (MUL atom
    | DIV atom)*
    ;

atom
    : INT
    ;
    
ID
    : ('a'..'z'
    | 'A'..'Z'
    | '_') ('a'..'z'
    | 'A'..'Z'
    | '0'..'9'
    | '_')*
    ;

INT
    : ('0'..'9')+
    ;
    
PLUS
    : '+'
    ;
    
MINUS
    : '-'
    ;
    
MUL
    : '*'
    ;
    
DIV
    : '/'
    ;
    
NL
    : '\n'
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS 
    : ( ' '
    | '\t'
    | '\r') {$channel=HIDDEN;}
    ;

